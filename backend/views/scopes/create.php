<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Scope */

$this->title = 'Добавить новую запись';
$this->params['breadcrumbs'][] = ['label' => 'Сферы деятельности', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scope-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
