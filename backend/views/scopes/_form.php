<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Scope */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="scope-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'image')->fileInput()
        ->hint((!empty($model->image)) ? Html::img($model->imageUrl, ['width'=>200]) : '') ?>
    <?= $form->field($model, 'active')->radioList([ 'Y' => 'Да', 'N' => 'Нет', ], ['separator' => '<br>']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
