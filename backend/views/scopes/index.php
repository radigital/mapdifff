<?php

use himiklab\sortablegrid\SortableGridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сферы деятельности';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scope-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Новая запись', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'title',
            ['format' => ['image', ['width'=>200]],
                'value'=>function($data) { return !empty($data->image) ? $data->imageUrl : false; },
            ],
            ['format' => 'html',
                'value'=>function($data) { return $data->active == 'Y' ? 'Да' : 'Нет'; },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
