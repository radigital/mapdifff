<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Scope */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Сферы деятельности', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="scope-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            [
                'label' => $model->getAttributeLabel('image'),
                'value'=> !empty($model->image) ? $model->imageUrl : false,
                'format' => ['image', ['width'=>200]],
            ],
            [
                'label' => $model->getAttributeLabel('active'),
                'value'=>$model->active == 'Y' ? 'Да' : 'Нет',
            ],
        ],
    ]) ?>

</div>
