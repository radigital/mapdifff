<?php
namespace frontend\controllers;

use common\models\Scope;
use Yii;
use frontend\models\RequestForm;
use yii\base\InvalidParamException;
use yii\web\NotFoundHttpException;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $layout = 'index';
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex($id = null)
    {
        if ($id) {
            $model = Scope::find()->active()->where('id=:id', ['id' => $id])->one();

            if (!$model) {
                throw new NotFoundHttpException();
            }
        } else {
            $model = Scope::find()->sorted()->one();
        }

        return $this->render('index', [
            'model' => $model
        ]);
    }

    public function actionForm()
    {
        $this->layout = 'inner';

        $model = new RequestForm();
        return $this->render('form', [
            'model' => $model,
        ]);
    }

    public function actionFormSubmit()
    {
        Yii::$app->response->format = 'json';

        $model = new RequestForm();

        $result = [
            'success' => true
        ];
        if (!$model->load(Yii::$app->request->post()) || !$model->validate() || !$model->sendEmail()) {
            $result = [
                'success' => false,
                'errors' => array_keys($model->errors)
            ];
        }

        return $result;
    }

    public function actionFormComplete()
    {
        $this->layout = 'inner';
        return $this->render('complete');
    }
}
