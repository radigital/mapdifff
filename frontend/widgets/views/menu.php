<?php
/**
 * @var $this frontend\widgets\MenuWidget
 * @var $items array
 */
?>
<menu>
    <? foreach ($items as $item): ?>
    <li><a href="<?= $item['url'] ?>"<?= ($item['active'] ? ' class="active"' : '') ?>><?= $item['title'] ?></a></li>
    <? endforeach; ?>
</menu>