<?php

namespace frontend\widgets;

use common\models\Scope;
use Yii;
use yii\bootstrap\Widget;
use yii\helpers\Url;

/**
 * Class MenuWidget
 * @package frontend\widgets
 */
class MenuWidget extends Widget
{
    public function run()
    {
        $scopes = Scope::find()->active()->sorted()->all();

        if (empty($scopes)) {
            return null;
        }

        $id = null;
        if ($this->isMapSpace) {
            $id = Yii::$app->request->get('id');
            if (!$id) {
                $id = $scopes[0]->id;
            }
        }

        $items = [];
        foreach ($scopes as $scope) {
            /** @var $scope Scope */
            $items[] = [
                'title' => $scope->title,
                'url' => Url::to(['site/index', 'id' => $scope->id]),
                'active' => $scope->id == $id,
            ];
        }

        return $this->render('menu', ['items' => $items]);
    }

    protected function getIsMapSpace()
    {
        return Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index';
    }
}