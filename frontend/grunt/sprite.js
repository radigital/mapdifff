module.exports =  {
	all: {
		src: '<%= dirs.template %>/images/src/*.png',
		dest: '<%= dirs.assets %>/images/spritesheet.png',
		destCss: '<%= dirs.assets %>/less/spritesheet.less',
		imgPath: '/assets/images/spritesheet.png',
		algorithm: 'binary-tree',
		engine: 'pngsmith'
	}
};