module.exports =  {
	main: {
		options: {
			compress: true,
			sourceMap: true,
			sourceMapFilename: "<%= dirs.assets %>/styles.min.css.map",
			sourceMapURL: "styles.min.map",
			sourceMapRootpath: "/"
		},
		src: [
			'<%= dirs.template %>/styles/styles.less'
		],
		dest: '<%= dirs.assets %>/css/styles.min.css'
	}
};
