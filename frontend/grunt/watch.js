module.exports =  {
	options: {
		debounceDelay: 100,
		livereload: true
	},
	default: {
		files: [
			'<%= dirs.template %>/styles/**/*.less',
			'<%= dirs.template %>/scripts/**/*.js'
		],
		tasks: 'default'
	}
};