module.exports =  {
	all: {
		options: {
			sourceMap: true
		},
		src: [
			'../vendor/bower/jquery/dist/jquery.min.js',
			'<%= dirs.template %>/scripts/**/*.js'
		],
		dest: '<%= dirs.assets %>/js/scripts.js'
	}
};