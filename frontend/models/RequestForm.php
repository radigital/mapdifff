<?php

namespace frontend\models;

use common\models\User;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class RequestForm extends Model
{
    public $company;
    public $work;
    public $address;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company', 'work', 'address'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company' => 'Компания',
            'work' => 'Сфера деятельности',
            'address' => 'Адрес',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        $users = User::find()->all();

        $messages = [];
        foreach ($users as $user) {
            /** @var $user User */
            if (empty($user->email)) {
                continue;
            }
            $messages[] = Yii::$app->mailer->compose('request', [ 'model' => $this ])
                ->setTo($user->email)
                ->setFrom([Yii::$app->params['sendEmail'] => Yii::$app->params['siteName']])
                ->setSubject('Поступила новая заявка');
        }

        return Yii::$app->mailer->sendMultiple($messages);
    }
}
