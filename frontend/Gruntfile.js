module.exports = function(grunt) {
	require('load-grunt-config')(grunt, {
		loadGruntTasks: {
			pattern: ['grunt-*']
		},
		config: {
			pkg: grunt.file.readJSON('package.json'),
            dirs: {
                template: 'web',
                assets: 'web/assets'
            }
		}
	});
};