<?php
/* @var $this \yii\web\View */
/* @var $content string */
?>
<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<div class="index-page">
    <?= $this->render('_head') ?>
    <div id="content">
        <?= $content ?>
    </div>
    <?= $this->render('_footer') ?>
</div>
<?php $this->endContent(); ?>