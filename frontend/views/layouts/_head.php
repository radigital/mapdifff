<?php
/* @var $this \yii\web\View */
use frontend\widgets\MenuWidget;
use yii\helpers\Url;

?>
<div id="head">
    <div class="wrap">
        <a class="logo" href="/"><img src="/images/logo.jpg" alt="Дикий филин"/></a>
        <a class="send-button" href="/<?= Url::to('site/form') ?>/">Поставь свою метку</a>
        <?= MenuWidget::widget() ?>
    </div>
</div>