<?php
/* @var $this \yii\web\View */
?>
<div id="footer">
    <div class="wrap">
        <a class="copyright" href="http://radigital.ru/" target="_blank">Собрано в RA Digital</a>
        <div class="social" data-url="<?=Yii::$app->request->hostInfo?>" data-img="<?=Yii::$app->request->hostInfo?>/images/logo.jpg" data-title="Это заголовок" data-desc="Это надпись поменьше">
            <a href="#" class="vk" title="Вконтакте"></a><a href="#" class="fb" title="Facebook"></a>
        </div>
    </div>
</div>