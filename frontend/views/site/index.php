<?php
/* @var $this yii\web\View */
/* @var $model common\models\Scope */
$this->title = !empty($model->title) ? $model->title : Yii::$app->params['siteName'];
?>
<div id="map">
    <img src="<?=!empty($model->image) ? $model->imageUrl : '/images/map.png' ?>" alt=""/>
</div>