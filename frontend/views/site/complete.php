<?php
/* @var $this yii\web\View */
$this->title = 'Ваша заявка успешно отправлена';
?>
<div id="complete">
    <div class="message">Отлично! Скоро ваша компания окажется на карте!</div>
    <div class="social" data-url="<?=Yii::$app->request->hostInfo?>" data-img="<?=Yii::$app->request->hostInfo?>/images/logo.jpg" data-title="Это заголовок" data-desc="Это надпись поменьше">
        <div class="text">Рассказажите об этом своим друзьям.</div>
        <a href="#" class="vk" title="Вконтакте"></a><a href="#" class="fb" title="Facebook"></a>
    </div>
</div>