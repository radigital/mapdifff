<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\RequestForm */

$this->title = 'Поставь свою метку';
?>
<form id="form" method="POST" action="/<?= Url::to('site/form') ?>/">
    <div class="message">Заполните форму и отправьте, чтобы мы поставили метку</div>
    <div class="field">
        <?= Html::activeTextInput($model, 'company', ['placeholder' => $model->getAttributeLabel('company')]) ?>
    </div>
    <div class="field">
        <?= Html::activeTextInput($model, 'work', ['placeholder' => $model->getAttributeLabel('work')]) ?>
    </div>
    <div class="field">
        <?= Html::activeTextInput($model, 'address', ['placeholder' => $model->getAttributeLabel('address')]) ?>
    </div>
    <div class="field">
        <?= Html::submitButton('Отправить') ?>
    </div>
</form>
