(function($){

    var form = $('#form');
    form.on('submit', function(e){
        e.preventDefault();

        $.ajax({
            url: '/site/form-submit/',
            data: form.serialize(),
            dataType: 'json',
            type: 'POST'
        }).done(function(data){
            form.find('.error').removeClass('error');

            if (data.success) {
                window.location = '/site/form-complete/';
                return;
            }

            if (data.errors) {
                $.each(data.errors, function(n, name){
                    var field = form.find('input[name="RequestForm[' +name+ ']"]');
                    field.addClass('error');
                });
            }
        });
    });

    form.find('input').on('change', function(){
        $(this).removeClass('error');
    });

})(jQuery);