(function($){
    var Share = {
        vkontakte: function(purl, ptitle, pimg, text) {
            url = 'http://vkontakte.ru/share.php?';
            url += 'url=' + encodeURIComponent(purl);
            url += '&title=' + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&image=' + encodeURIComponent(pimg);
            url += '&noparse=true';
            Share.popup(url);
        },

        facebook_share: function(purl, ptitle, pimg, text) {
            url = 'https://www.facebook.com/sharer/sharer.php?s=100&app_id=1546544462258890';
            url += '&p[title]=' + encodeURIComponent(ptitle);
            url += '&p[summary]=' + encodeURIComponent(text);
            url += '&p[url]=' + encodeURIComponent(purl);
            url += '&p[images][0]=' + encodeURIComponent(pimg);
            Share.popup(url);
        },
        facebook_dlg: function(purl, ptitle, pimg, text) {
            url = 'https://www.facebook.com/dialog/feed?app_id=1546544462258890&&display=popup';
            url += '&link=' + encodeURIComponent(purl);
            url += '&picture=' + encodeURIComponent(pimg);
            //url += '&caption=' + encodeURIComponent (ptitle);
            url += '&name=' + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&redirect_uri=' + encodeURIComponent(purl);
            Share.popup(url);
        },
        twitter: function(purl, ptitle) {
            url = 'http://twitter.com/share?';
            url += 'text=' + encodeURIComponent(ptitle);
            url += '&url=' + encodeURIComponent(purl);
            url += '&counturl=' + encodeURIComponent(purl);
            Share.popup(url);
        },

        popup: function(url) {
            window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
        }
    };

    $('.social a').on('click', function(e) {
        e.preventDefault();
        var social = $(this).closest('.social'),
            title = social.data('title'),
            desc = social.data('desc'),
            img = social.data('img'),
            url = social.data('url');
        if ($(this).hasClass('vk')) Share.vkontakte(url, title, img, desc);
        else if ($(this).hasClass('fb')) Share.facebook_dlg(url, title, img, desc);
        else if ($(this).hasClass('tw')) Share.twitter(url, title + ' ' + desc);
    });
})(jQuery);