<?php

namespace common\models;

use common\components\SaveImageBehavior;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use himiklab\sortablegrid\SortableGridBehavior;

/**
 * This is the model class for table "scopes".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property string $active
 */
class Scope extends ActiveRecord
{
    /**
     * @inheritdoc
     * @return ScopeQuery
     */
    public static function find()
    {
        return new ScopeQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'sort' => [
                'class' => SortableGridBehavior::className(),
                'sortableAttribute' => 'order'
            ],
            'image' => [
                'class' => SaveImageBehavior::className(),
                'attributeName' => 'image',
                'savePath' => '@upload',
                'generateNewName' => true,
                'protectOldValue' => true,
                'resize' => '1920x1600'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scopes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'active'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['image'], 'file',
                'extensions' => 'jpg, jpeg, png',
                'mimeTypes' => 'image/jpeg, image/png',
                'maxSize' => 1024*1024*20
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'image' => 'Изображение',
            'active' => 'Активна',
        ];
    }

    public function getImageUrl()
    {
        return '/upload/' . $this->image;
    }
}