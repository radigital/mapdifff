<?php
namespace common\models;

use yii\db\ActiveQuery;

class ScopeQuery extends ActiveQuery
{
    public function sorted()
    {
        $this->orderBy('order');
        return $this;
    }

    public function active()
    {
        $this->andWhere("active='Y'");
        return $this;
    }
}