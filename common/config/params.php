<?php
return [
    'siteName' => 'Дикий филин',
    'sendEmail' => 'admin@example.com',
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
