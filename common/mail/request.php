<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\RequestForm */
?>

<h3>Здравствуйте!</h3>
<p>Поступила новая заявка на добавление компании:</p>
<p>
    <u><?= $model->getAttributeLabel('company') ?></u>: <?= Html::encode($model->company) ?> <br>
    <u><?= $model->getAttributeLabel('work') ?></u>: <?= Html::encode($model->work) ?> <br>
    <u><?= $model->getAttributeLabel('address') ?></u>: <?= Html::encode($model->address) ?> <br>
</p>
