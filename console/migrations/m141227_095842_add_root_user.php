<?php

use yii\db\Schema;
use yii\db\Migration;

class m141227_095842_add_root_user extends Migration
{
    public function up()
    {
        $this->insert('user', [
            'id'=>2,
            'username'=>'root',
            'auth_key'=>'pf1cvUFy6o6gd7T9vInetZX3yrufrkb8',
            'password_hash'=>'$2y$13$v98JxzzmSgenA7ZVfL3H3.kjPiGSA/L0teTTaujspTTb1aYRJwEyC',
            'status'=>'10',
        ]);
    }

    public function down()
    {
        $this->delete('user', "id=2");
    }
}
