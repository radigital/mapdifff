<?php

use yii\db\Schema;
use yii\db\Migration;

class m141221_152530_default_user extends Migration
{
    public function up()
    {
        $this->insert('user', [
            'id'=>'1',
            'username'=>'admin',
            'auth_key'=>'h30sxgvh87fEe399reqgMMev7hg5zyXq',
            'password_hash'=>'$2y$13$wyALfmUkHJkLBfwoc8EOzuiWdn6og1bq9q95BYU8ZktEVY2TCBw4a',
            'password_reset_token'=>'',
            'email'=>'admin@difff.ru',
            'status'=>'10',
            'created_at'=>'1419009424',
            'updated_at'=>'1419009424',
        ]);
    }

    public function down()
    {
        $this->delete('user', "id=1");
    }
}
