<?php

use yii\db\Schema;
use yii\db\Migration;

class m141221_095202_scopes extends Migration
{
    public function up()
    {
        $this->execute("
            CREATE TABLE `scopes` (
              `id` int(14) unsigned NOT NULL AUTO_INCREMENT,
              `title` varchar(255) NOT NULL,
              `image` varchar(255) NOT NULL,
              `active` enum('Y','N') NOT NULL DEFAULT 'Y',
              `order`  int(14) NOT NULL DEFAULT 0 ,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
        ");
    }

    public function down()
    {
        $this->dropTable('scopes');
    }
}
